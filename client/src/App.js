import React, {Component} from "react";
import PropTypes from "prop-types";
import { Provider, connect } from "react-redux";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import MovieSearch from "./components/movie/MovieSearch.jsx";
import ClearCache from "./components/cache/ClearCache.jsx";
import NotFound from "./components/notFound.jsx";
import Footer from "./components/footer/Footer.jsx";
import Header from "./components/header/Header.jsx";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";


class App extends Component {

  componentDidCatch(error) {
     console.error(error)
  }

  render() {
    const { locale } = this.props;
    return (
      <React.Fragment>
         <BrowserRouter>
            <div>
              <ToastContainer />
              <Header locale={locale}/>
              <main>
                <Switch>
                  <Route path="/clear-cache" component={ClearCache} />
                  <Route
                    path="/movies"
                    render={props => <MovieSearch {...props} />}
                  />
                  <Route path="/not-found" component={NotFound} />
                  <Redirect from="/" exact to="/movies" />
                  <Redirect to="/not-found" />
                </Switch>
              </main>
              <Footer />
            </div>
          </BrowserRouter>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return { locale: state.locale.localeValue };
};

export default connect(mapStateToProps)(App);


