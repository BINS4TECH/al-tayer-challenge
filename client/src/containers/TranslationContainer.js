import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { IntlProvider } from "react-intl";
import { fetchLocaleMessage } from "../utils/MessageUtil.js";

class TranslationContainer extends Component {
  render() {
    const { locale } = this.props;
    const message = fetchLocaleMessage(locale);
    return (
      <IntlProvider locale={locale} messages={message}>
        {this.props.children}
      </IntlProvider>
    );
  }
}

TranslationContainer.propTypes = {
  locale: PropTypes.string
};

const mapStateToProps = state => {
  return { locale: state.locale.localeValue };
};

export default connect(mapStateToProps)(TranslationContainer);
