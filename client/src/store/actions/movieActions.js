import { apiCallAction } from "../middleware/apiCallMiddleware.js";

export const MOVIE_SEARCH = "MOVIE_SEARCH";
export const MOVIE_SEARCH_INPROGRESS = "MOVIE_SEARCH_INPROGRESS";
export const MOVIE_SEARCH_SUCCESS = "MOVIE_SEARCH_SUCCESS";
export const MOVIE_SEARCH_FAILURE = "MOVIE_SEARCH_FAILURE";
export const fetchMoviesAPI = searchQuery => {
 const URL = `/api/movies?searchQuery=${encodeURIComponent(
  searchQuery
)}`;
  return apiCallAction({
    type: MOVIE_SEARCH,
    payload: {
      searchQuery
    },
    apiCall: (api, { searchQuery }) => api.get(URL).then(res => res.data),
    options: {
      showProgress: true,
      fromCache: true,
      notificationType: MOVIE_SEARCH_INPROGRESS
    }
  });
};
