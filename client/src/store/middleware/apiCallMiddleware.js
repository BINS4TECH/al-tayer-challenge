export function apiCallAction(definition) {
  return definition;
}

export function apiCallMiddleware(api) {
  let storeCache = [];
  return ({ dispatch, getState }) => next => action => {
    const { type, apiCall, payload, options } = action;
    if (apiCall) {
      const state = getState();
      const cache = storeCache.find(
        cacheObj => cacheObj.searchQuery === payload.searchQuery
      );
      if (
        options &&
        options.fromCache &&
        cache &&
        Date.now() - cache.timestamp > 3000
      ) {
        const response = cache.searchResults;
        dispatch(
          Object.assign({ type: type + "_SUCCESS" }, payload, {
            response,
            options,
            oldState: state
          })
        );
        return;
      }
      if (
        options &&
        options.showProgress === true &&
        options.notificationType
      ) {
        dispatch({ type: options.notificationType, ...payload });
      }
      const request = apiCall(api, payload)
        .then(response => {
          return response;
        })
        .catch(error => {
          throw error;
        });

      return request
        .then(response => {
          if (options && options.fromCache) {
            storeCache.push({
              timestamp: Date.now(),
              searchQuery: payload.searchQuery,
              searchResults: response
            });
          }
          dispatch(
            Object.assign({ type: type + "_SUCCESS" }, payload, {
              response,
              options,
              oldState: state
            })
          );
          return response;
        })
        .catch(error => {
          dispatch(
            Object.assign({ type: type + "_FAILURE" }, payload, {
              error,
              options,
              oldState: state
            })
          );
          throw error;
        });
    }
    return next(action);
  };
}
