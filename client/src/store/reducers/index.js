import { combineReducers } from 'redux';
import movieReducer from './movieReducer.js';
import localeReducer from './localeReducer.js';

const rootReducer = combineReducers({
  movies: movieReducer,
  locale: localeReducer
});

export default rootReducer;
