import React from "react";

const Footer = () => {
  return (
    <React.Fragment>
      <footer className="app-footer">
        <div className="text-center py-3">
          © With love:
          <a href="https://www.altayer.com/"> Al Tayer group</a>
        </div>
      </footer>
    </React.Fragment>
  );
};

export default Footer;
