import React, { Component } from "react";
import http from "../../services/httpService";

const URL = `/api/cache/refresh`;
class ClearCache extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msg: "Please wait...."
    };
  }

  async componentDidMount() {
    console.log("calling clear cache api...");
    try {
      const response = await http.get(URL);
      const msg = response && response.data ? response.data : "Server error";
      this.setState({ msg });
    } catch (error) {
      this.setState({ msg: "API call error" });
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 search-container">
              <div className="row">
                <p className="lead">{`${this.state.msg}`}</p>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ClearCache;
