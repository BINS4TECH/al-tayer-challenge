import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { injectIntl } from "react-intl";

class Header extends Component {
  render() {
    const { dispatch, intl } = this.props;
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-lg navbar-dark bg-info">
          <Link className="navbar-brand" to={"/"}>
            {intl.formatMessage({ id: "MOVIE_SEARCH" })}
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item ">
                <Link to={"/clear-cache"} className="nav-link">
                  {intl.formatMessage({ id: "CLEAR_CACHE" })}
                </Link>
              </li>
            </ul>
          </div>
          <form className="form-inline my-2 my-lg-0">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item ">
                <a
                  className="nav-link"
                  onClick={() =>
                    dispatch({ type: "CHANGE_LOCALE_VALUE", payload: "en-US" })
                  }
                >
                  <img
                    width="30"
                    height="30"
                    src={require("../../img/USA.png")}
                    alt=""
                  />
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="btn nav-link"
                  onClick={() =>
                    dispatch({ type: "CHANGE_LOCALE_VALUE", payload: "ar-AE" })
                  }
                >
                  <img
                    width="30"
                    height="30"
                    src={require("../../img/UAE.png")}
                    alt=""
                  />
                </a>
              </li>
            </ul>
          </form>
        </nav>
      </React.Fragment>
    );
  }
}
export default connect()(injectIntl(Header));
