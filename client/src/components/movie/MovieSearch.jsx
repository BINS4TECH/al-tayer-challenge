import React, { Component } from "react";
import { connect } from "react-redux";
import SearchBox from "./SearchBox.jsx";
import { fetchMoviesAPI } from "../../store/actions/movieActions";
import MovieList from "./MovieList.jsx";

const WAIT_INTERVAL = 300;
const ENTER_KEY = 13;
const MINIMUM_SEARCH_STRING_LENGTH = 3;

class MovieSearchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: ""
    };
    this.timerID = null;
  }

  fetchMoviesFromServer = async () => {
    const { fetchMovies } = this.props;
    fetchMovies(this.state.searchTerm);
  };

  handleKeyDown = e => {
    if (e.keyCode === ENTER_KEY) {
      clearTimeout(this.timerID);
      this.searchMovies();
    }
  };

  // Calls when user clicks on search button
  searchMovies = event => {
    event.preventDefault();
    console.log(this.props);
    if (this.timerID) {
      clearTimeout(this.timerID);
    }
    if (this.state.searchTerm.length >= MINIMUM_SEARCH_STRING_LENGTH) {
      this.fetchMoviesFromServer();
    }
  };

  // calls when user types...
  setSearchQuery = searchTerm => {
    if (this.timerID) {
      clearTimeout(this.timerID);
    }
    this.setState({ searchTerm });
    if (searchTerm.length >= MINIMUM_SEARCH_STRING_LENGTH) {
      this.timerID = setTimeout(this.fetchMoviesFromServer, WAIT_INTERVAL);
    }
  };

  showUserMessage = () => {
    const { userMessage } = this.props;
    if (this.state.searchTerm.length < 3) {
      return `Please enter 3 or more characters to start searching movies`;
    }
    return userMessage;
  };

  render() {
    const { movies } = this.props;
    console.log(movies);
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 search-container">
              <SearchBox
                value={this.state.searchTerm}
                onChange={this.setSearchQuery}
                onSearch={this.searchMovies}
                placeHolder={`Please enter 3 or more characters to start searching movies`}
              />
              <div className="mx-auto" style={{ marginTop: "20px" }}>
                <p>{this.showUserMessage()}</p>
              </div>
            </div>
            {this.state.searchTerm.length >= 3 &&
              Array.isArray(movies) && <MovieList movies={movies} />}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => ({
  searchQuery: state.movies.searchQuery,
  movies: state.movies.searchResults,
  userMessage: state.movies.userMessage
});

const mapDispatchToProps = dispatch => ({
  fetchMovies: async searchQuery => {
    await dispatch(fetchMoviesAPI(searchQuery));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieSearchForm);
