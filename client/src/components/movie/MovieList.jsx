import React from "react";
import PropTypes from "prop-types";
import MovieCard from "./MovieCard.jsx";

const getMovies = movies => {
  if(!movies || movies.length === 0) {
    return (
      <React.Fragment>
       <span> Movie list is empty </span>
      </React.Fragment>
    );
  }
  return (
    <React.Fragment>
      {movies.map(movie => (
        <MovieCard key={movie.imdbID} movie={movie} />
      ))}
    </React.Fragment>
  );
};

const MovieList = ({ movies }) => (
  <React.Fragment>{getMovies(movies)}</React.Fragment>
);

MovieList.defaultProps = {
  movies: []
};

MovieList.propTypes = {
  movies: PropTypes.array
};

export default MovieList;
