import React from "react";
import PropTypes from "prop-types";

const SearchBox = ({
  value,
  onChange,
  placeHolder = "Please enter search term.",
  onSearch = e => e.preventDefault()
}) => {
  return (
    <React.Fragment>
      <div className="row">
        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <div className="search-box">
            <form className="search-form">
              <input
                type="text"
                name="searchInput"
                className="form-control"
                placeholder={placeHolder}
                value={value}
                onChange={e => onChange(e.currentTarget.value)}
              />
              <button className="btn btn-link search-btn" onClick={onSearch}>
                <i className="glyphicon glyphicon-search" />
              </button>
            </form>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

SearchBox.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeHolder: PropTypes.string,
  onSearch: PropTypes.func
};

export default SearchBox;
