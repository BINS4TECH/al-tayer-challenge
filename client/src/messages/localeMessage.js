export default {
  'en-US': {
    'HOME': 'Home',
    'RESULT': 'Result',
    'MOVIE_SEARCH': 'Movie search',
    'CLEAR_CACHE': 'Clear cache'
  },
  'ar-AE': {
    'HOME': 'الصفحة الرئيسية',
    'RESULT': 'النتائج',
    'MOVIE_SEARCH': 'البحث عن فيلم',
    'CLEAR_CACHE':'مسح ذاكرة التخزين المؤقت'
  }
}
