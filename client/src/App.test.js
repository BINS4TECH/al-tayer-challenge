import { mount } from "enzyme";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import ar from "react-intl/locale-data/ar";
import * as React from "react";
import { createStore } from "redux";
import App from "./App";

const sleep = delay => {
  return new Promise(resolve => setTimeout(resolve, delay));
};

addLocaleData([...en, ...ar]);
const emptyState = {
  movies: {
    searchQuery: "",
    searchResults: null,
    userMessage: ""
  },
  locale: { localeValue: "en-US" }
};
async function render({ url = "/" }) {
  const store = createStore({
    ...emptyState
  });
  const wrapper = mount(<App store={store} />);
  await sleep(0);
  return { store, wrapper };
}

it("should render movie search container)", async () => {
  const { wrapper } = await render({ url: "/" });
  expect(wrapper.contains(<p>Please enter 3 or more characters to start searching movies</p>)).toBe(true);
});
