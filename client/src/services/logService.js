// Configure raven or any thirdparty logging library.

function init() {}

function log(error) {
  console.error(error);
}

export default {
  init,
  log
};
