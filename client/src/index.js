import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import ar from "react-intl/locale-data/ar";
import App from "./App";
import TranslationContainer from "./containers/TranslationContainer.js";
import registerServiceWorker from "./registerServiceWorker";
import http from "./services/httpService";
import rootReducer from "./store/reducers";
import { apiCallMiddleware } from "./store/middleware/apiCallMiddleware";
import "./index.css";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";

const apiMiddleware = apiCallMiddleware(http);

const store = createStore(rootReducer, applyMiddleware(apiMiddleware));

addLocaleData([...en, ...ar]);

ReactDOM.render(
  <Provider store={store}>
    <TranslationContainer>
      <App />
    </TranslationContainer>
  </Provider>,

  document.getElementById("root")
);

registerServiceWorker();
