# create-react-app React Project with Node Express Backend

> React app with a Node Express Backend

## Usage

Install [nodemon](https://github.com/remy/nodemon) globally

```
npm i nodemon -g
```

Install server and client dependencies

```
yarn
cd client
yarn
```

To start the server and client at the same time (from the root of the project)

```
yarn dev
```

Running the production build on localhost. This will create a production build, then Node will serve the app on http://localhost:3900

```
NODE_ENV=production yarn dev:server
```

## Client side implementaion

1. I used Redux as a state managent system and used a middleware to handle and monitor the user actions. If User invokes any API call to server, then this middleware handles all the cases(Inprogress, Success, Failure). This helped me to write a less code in the project as it is a generic function which dispatches inprogress, success, failure events. Using this aproach we get a full control of showing nice messages to user. We can add a notification bar or toast notification container in our component hierrarchy and this component can decide how to display the messages and what time. I used this middleware function varaible as cache as it is in store.


## Server side implementaion.

I used express framework in this application. Here also I used a middleware function to store the data and update the data to cache.

This middleware function gets the user requests first When they invoke a API call. I will check whether the search string is present in the cache or not. If the key is not there then I update the cache and pass to next chain so routes always fetch the data from the cache. I used some logic to handle the concurrent requests with same query and calls only one time to the external server.If the cache is less than one minute then I update the cahe using setTimeout function.

**I have tried to cover all the points that mentioned in the challenge in a generic way and created my own implementation for caching and concurrent request processing**


#  The time you spent on the challenge
I could not work on this challenge till Sunday as my home town was going through a serious natural calamities like flooding and raining(https://en.wikipedia.org/wiki/2018_Kerala_floods) and now we are slowly recovering from it. So I have started to work on this from Monday and spent 4 hours daily as I am a full time employee with Merck and took around 16 hours to finish the design, implementation and testing.


# What would you change in your submission to make it production ready?
 I used config npm module to handle dev, test and production environment so we need to add environment specific varaibles either in config or system environment variable or through command line.

```
NODE_ENV=production yarn dev:server
```

# What would you do differently if you had more time?
I would spend time on styling the react components, improvement of image loading and performance optimization.


