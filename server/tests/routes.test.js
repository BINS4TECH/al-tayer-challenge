const request = require("supertest");
let server;
// yarn run test
describe("/api/movies, /api/cache/", () => {
  beforeEach(() => {
    server = require("../server");
  });
  afterEach(async () => {
    await server.close();
  });

  describe("Fetch movies , cache update..", () => {
    it("should not update cache as there is no items in the cache", async () => {
      const res = await request(server).get(
        "/api/cache/refresh"
      );
      expect(res.status).toBe(200);
      expect(res.text).toEqual("No keys in the cache");
    });

    it("should return 20 movie names with search term = action", async () => {
      const res = await request(server).get("/api/movies?searchQuery=action");
      expect(res.status).toBe(200);
      expect(res.body.data.length).toBe(20);
   });

   it("should not return any movies with search term = $$GGGASDEWDDDDqwwewe@#$$$", async () => {
     const res = await request(server).get(
       "/api/movies?searchQuery=$$GGGASDEWDDDDqwwewe@#$$$"
     );
     expect(res.status).toBe(200);
     expect(res.body.data.length).toBe(0);
   });

    it("should update cache..", async () => {
      const res = await request(server).get(
        "/api/cache/refresh"
      );
      expect(res.status).toBe(200);
      expect(res.text).toEqual("Cache updated");
    });
  });
});
