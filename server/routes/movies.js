const express = require("express");
const winston = require("winston");
const http = require("../util/httpUtil");
const router = express.Router();
//const redis = require('redis');
/* for large scale applications we can use redis 
but for this simple movie app I am using ES6 Map
// const client = redis.createClient(); */
const movieCache = new Map();
const runningRequests = new Map();
const cacheMiddleWare = require("../middleware/cacheMiddleware")(
  movieCache,
  runningRequests
);
router.get("/movies", [cacheMiddleWare], async (req, res, next) => {
  const { searchQuery } = req.query;
  try {
    const cachedData = movieCache.get(searchQuery);
    if (cachedData != null) {
      winston.info("from cache1");
      if (cachedData.searchResults && cachedData.searchResults.length > 0) {
        res.set("Cache-Control", "public, max-age=30");
      }
      res.json({ data: cachedData.searchResults });
    } else {
      /* it should not come here as I am setting cache first and coming
       here for handling concurrent requests */
      res.status(400).send("Error");
    }
  } catch (error) {
    winston.error(error);
    next(error);
  }
});

router.get("/cache/refresh", async (req, res) => {
  if (movieCache.size === 0) {
    res.set("Surrogate-Control", "no-store");
    res.set(
      "Cache-Control",
      "no-store, no-cache, must-revalidate, proxy-revalidate"
    );
    res.set("Pragma", "no-cache");
    res.set("Expires", "0");
    res.send("No keys in the cache");
  } else {
    const refreshValue = async (value, key) => {
      const searchResults = await http.fetchMovies({ searchQuery: key });
      const cacheObj = {
        searchResults,
        timestamp: Date.now()
      };
      movieCache.set(key, cacheObj);
    };
    movieCache.forEach(refreshValue);
    res.set("Surrogate-Control", "no-store");
    res.set(
      "Cache-Control",
      "no-store, no-cache, must-revalidate, proxy-revalidate"
    );
    res.set("Pragma", "no-cache");
    res.set("Expires", "0");
    res.send("Cache updated");
  }
});

module.exports = router;
